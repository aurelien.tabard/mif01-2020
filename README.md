# Gestion de Projet et Génie Logiciel, M1, département informatique, Lyon 1

## Dates importantes

<!-- [Cf. ADE](http://adelb.univ-lyon1.fr/direct/index.jsp?projectId=4&days=0,1,2,3,4&resources=33140&weeks=4,5). -->

* Emploi du temps : à venir 
  Attention : les groupes ne sont pas correctement entrés dans ADE.
  Utilisez la ressource "M1 informatique" pour voir l'emploi du temps
  (pas grA, grB, ...).

* Rendu du TP noté : dimanche 20 septembre 2020 à 23h59. Voir
  [projet-note.md](projet-note.md) pour un récapitulatif des consignes.

* Examen : date à venir. 1h30 (sauf tiers-temps) Consignes : Seules 5
  feuilles A4 recto-verso (donc 10 pages au total) sont autorisées à
  l’examen. Leur contenu est libre. Elles peuvent être une sélection de
  transparents, ou manuscrites, avec une taille de caractère de votre
  choix. Les annales de l'examen sont dans le répertoire [exam/](exam/)
  et [sur l'ancienne page du
  cours](http://www.tabard.fr/cours/2017/mif01/). Prévoyez un stylo
  bleu foncé ou noir, et un blanc correcteur.

Barème : 50% examen / 50% TP.

## Groupes

La promotion est divisée en deux moitiés : A+B+C et D+E+F (cf. TOMUSS). La plupart du temps (mais pas toujours), quand la moitié A+B+C est en gestion de projet et génie logiciel, l'autre moitié est en programmation avancée, et vice-versa. Chaque moitié est divisée en groupes : A1, A2, B1, B2, ... L'affectation des groupes aux salles est faite dans cet agenda :

https://calendar.google.com/calendar/embed?src=bujrk2sa90qim7u9okmo405b6s%40group.calendar.google.com&ctz=Europe%2FParis

## Enseignants et contacts

* Demi-promo A+B+C : [Aurélien Tabard](http://www.tabard.fr/), [Matthieu Moy](https://matthieu-moy.fr/) (responsable du cours), Florian Adam (email : prénom.nom@cgi.com)

* Demi-promo D+E+F : [Lionel Medini](https://perso.liris.cnrs.fr/lionel.medini/), [Thomas Bonis](https://liris.cnrs.fr/page-membre/thomas-bonis), [Paul Iannetta](https://perso.ens-lyon.fr/paul.iannetta/research/) (email : prénom.nom@ens-lyon.fr)

(Lorsque ce n'est pas précisé, l'email prenom.nom@univ-lyon1.fr fonctionne)

## Nouvelles du cours

Les informations de dernière minute sont disponibles ici :
[NEWS.md](NEWS.md). Les informations importantes seront envoyées par
email, ce fichier en contient une copie.

## CM1 1: Introduction

### Intro du cours

À venir

### Introduction au génie logiciel

* Vidéo : [<img src="https://mediacenter.univ-lyon1.fr/videos/MEDIA200812122241525/preview.jpg" width="128" height="72" />](https://mediacenter.univ-lyon1.fr/videos/?video=MEDIA200812122241525)

* Transparents : [01-introduction-slides.pdf](01-introduction-slides.pdf) (version imprimable : [01-introduction-handout.pdf](01-introduction-handout.pdf))
  
## TP1 1 : Mise en route Java

* [TP1-java/README.md](TP1-java/README.md)

## CM 2 : Outillage : Maven, la forge Gitlab, intégration continue, coding style ...

### Maven, Forge, Intégration Continue

* Vidéo : [<img src="https://mediacenter.univ-lyon1.fr/videos/MEDIA200812085007594/preview.jpg" width="128" height="72" />](https://mediacenter.univ-lyon1.fr/videos/?video=MEDIA200812085007594)

* Transparents : [02-cm-maven-forge-ic-slides.pdf](02-cm-maven-forge-ic-slides.pdf)
  (Version imprimable : [02-cm-maven-forge-ic-handout.pdf](02-cm-maven-forge-ic-handout.pdf))

## TP 2 :

* [TP2-outils/README.md](TP2-outils/README.md)

## CM 3, 4 & 5 : Coding style, Design-patterns

### Coding style

* Vidéo : [<img src="https://mediacenter.univ-lyon1.fr/videos/MEDIA200812151525896/preview.jpg" width="128" height="72" />](https://mediacenter.univ-lyon1.fr/videos/?video=MEDIA200812151525896)

* Transparents : [03-coding-style-slides.pdf](03-coding-style-slides.pdf)
  (version imprimable : [03-coding-style-handout.pdf](03-coding-style-handout.pdf))

### Design patterns

* Slides : [CM-patterns.pdf](https://perso.liris.cnrs.fr/lionel.medini/enseignement/MIF01/CM-patterns.pdf)

## TP 3 & 4 : Design patterns & refactoring

* [TP3-patterns/README.md](TP3-patterns/README.md) et de l'aide avec [MVC](TP3-patterns/mvc.md)

## CM 5 : Specifications et cas d'utilisation

* [05-UML-CU.pdf](05-UML-CU.pdf)

## CM 6 : Test

À venir.
<!-- 
* Transparents de Sandrine Gouraud :
  [06-expose18092019.pdf](06-expose18092019.pdf)
  
* [Software Fail Watch, 5th
  edition](https://www.tricentis.com/wp-content/uploads/2018/01/20180119_Software-Fails-Watch_Small_Web.pdf)
  présenté pendant le cours. -->

## CM 7 : Métaprogrammation, introduction à l'agilité

### Métaprogrammation

* Vidéo : Vidéo : [<img src="https://mediacenter.univ-lyon1.fr/videos/MEDIA200814143643906/preview.jpg" width="128" height="72" />](https://mediacenter.univ-lyon1.fr/videos/?video=MEDIA200814143643906)

* Transparents [07-metaprogramming-slides.pdf](07-metaprogramming-slides.pdf)
  (version imprimable : [07-metaprogramming-handout.pdf](07-metaprogramming-handout.pdf))

### Introduction à l'Agilité

* Vidéo : Vidéo : [<img src="https://mediacenter.univ-lyon1.fr/videos/MEDIA200814142723170/preview.jpg" width="128" height="72" />](https://mediacenter.univ-lyon1.fr/videos/?video=MEDIA200814142723170)

* Transparents : [08-agilite-slides.pdf](08-agilite-slides.pdf)
  (version imprimable : [08-agilite-handout.pdf](08-agilite-handout.pdf))

### Intervention de Levent Acar

À venir.

## TD 1 : Cas d'utilisation et coding styles

* [TD1-uc-style/TD_UseCaseFffound_et_style.pdf](TD1-uc-style/TD_UseCaseFffound_et_style.pdf)
  (corrigé : [TD1-uc-style/TD_UseCaseFffound_et_style_correction.pdf](TD1-uc-style/TD_UseCaseFffound_et_style_correction.pdf))

## TP 6 & 7 : tests

* [TP4-tests/README.md](TP4-tests/README.md)

## CM 8 : Agilité (suite)

* Transparents : [08-agilite-slides.pdf](08-agilite-slides.pdf)
  (version imprimable : [08-agilite-handout.pdf](08-agilite-handout.pdf))

## TD 2: Paper4Scrum

* [TD2-scrum/ExerciceAgile.pdf](TD2-scrum/ExerciceAgile.pdf)

## CM 8 : Éthique

* Vidéo : Vidéo : [<img src="https://mediacenter.univ-lyon1.fr/videos/MEDIA200817132953478/preview.jpg" width="128" height="72" />](https://mediacenter.univ-lyon1.fr/videos/?video=MEDIA200817132953478)

* Transparents : [05-ethics-slides.pdf](05-ethics-slides.pdf)
  (version imprimable : [05-ethics-handout.pdf](05-ethics-handout.pdf))

## CM 10: Gestion de projet en entreprise

Intervention de Jonathan BENZAQUEN.

# How to use this repository

This repository contains course material and code skeletons. The
recommended way to use it is:

* Fork the project from
  https://forge.univ-lyon1.fr/matthieu.moy/m1if01-2019 and make it private.
  
* Clone your fork and work in the fork.

To get updates from the teacher's repository, run once :

    git remote add moy https://forge.univ-lyon1.fr/matthieu.moy/m1if01-2019.git

Then, each time you want to fetch updates, run :

    git pull moy master

This downloads and applies changes made by the teachers. You can keep
working as usual with your private repository. In summary:

    git pull            # get changes from your private fork (i.e. your co-worker)
    git push            # send changes to your private fork
    git pull moy master # get updates from teachers
